import React from "react"

export default function Navbar() {
  return (
    <nav className="main-header navbar navbar-expand navbar-white navbar-light">
      <ul className="navbar-nav">
        <li className="nav-item">
          <a href="" className="nav-link">
            <i className="fas fa-bars" />
          </a>
        </li>
        <li className="nav-item">
          <a href="" className="nav-link">
            Inicio
          </a>
        </li>
        <li className="nav-item">
          <a href="" className="nav-link">
            Productos
          </a>
        </li>
      </ul>
    </nav>
  )
}
