import React from "react"

import "./maintenance.css"

export default function Maintenance() {
  return (
    <div className="maintenance">
      <div className="maintenance_contain">
        <img
          src="https://demo.wpbeaveraddons.com/wp-content/uploads/2018/02/main-vector.png"
          alt="maintenance"
        />
        <span className="pp-infobox-title-prefix">PROXIMAMENTE ACA PODRAS COMPRAR ARTICULOS DE MASCOTAS</span>
        <div className="pp-infobox-title-wrapper">
          <h3 className="pp-infobox-title">El sitio web se encuentra en construccion</h3>
        </div>
     
      </div>
    </div>
  )
}
