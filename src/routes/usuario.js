const { Router } = require('express')
const {crearUsuario, getUsuarios, getUsuario, modificarUsuario, borrarUsuario} = require("../controllers/usuario")
const verificarToken = require("../middlewares/auth");


const routerUsuario = Router()

routerUsuario.post("", [], crearUsuario)
routerUsuario.get("/all", [verificarToken], getUsuarios)
routerUsuario.get("", [verificarToken], getUsuario)
routerUsuario.put("", [verificarToken], modificarUsuario)
routerUsuario.delete("", [verificarToken], borrarUsuario)

module.exports = routerUsuario