const {Router} = require("express")
const { login, validar } = require("../controllers/auth")
const verificarToken = require("../middlewares/auth")


const router = Router()

router.post("/login", login)
router.get("/validar", [verificarToken], validar)

module.exports = router