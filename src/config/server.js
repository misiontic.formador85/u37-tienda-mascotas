const express = require("express")
const routerProducto = require("../routes/producto")
const routerUsuario = require("../routes/usuario")
const connectDB = require("./database")
const fileUpload = require("express-fileupload")
const cors = require("cors")
const dotenv = require("dotenv").config()
const path = require("path")

class Server {
  constructor() {
    this.aplicacion = express()
    this.aplicacion.listen(process.env.PORT, () => {
      console.log("se esta ejecutando el servidor")
    })
    this.middlewares()
    this.rutas()
    connectDB()
  }

  middlewares() {
    // Middlewares
    this.aplicacion.use(express.json())
    this.aplicacion.use(
      fileUpload({
        useTempFiles: true,
        tempFileDir: "/tmp/",
      })
    )
    this.aplicacion.use(cors())
    this.aplicacion.use(express.static("public"))
  }

  rutas() {
    this.aplicacion.use("/usuario", routerUsuario)
    this.aplicacion.use("/producto", routerProducto)
    this.aplicacion.use("/auth", require("../routes/auth"))
    this.aplicacion.get("*", (req, res) => {
      res.sendFile(path.join(__dirname,"../../public/index.html"))
    })
    
  }
}

module.exports = Server
