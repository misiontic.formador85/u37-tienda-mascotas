const {connect} = require("mongoose")
const dotenv = require("dotenv").config()

// 1. Conectamos la base de datos
const connectDB = () => {
  connect(process.env.URL_DATABASE , {
    useNewUrlParser: true,
    })
    .then(() => {
      console.log("conectado a la base de datos")
    })
    .catch(() => {
      console.error("no se logro conectar con la base de datos")
    })
}

module.exports = connectDB